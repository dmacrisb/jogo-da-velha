var p1 = "Aqua";
var p2 = "Magma";
var turn = p1;
var gameover = false;
var ptp1 = 0;
var ptp2 = 0;

main();

function main(){
    updateScore();
    actionOnTable();
    restartMatch();
}


async function endgame(){
    // Cria matriz e salva valores
    var winner = "";
    var draw = false;
    var mat = [["","",""],["","",""],["","",""]];
    mat[0][0] = document.getElementById("a1").getAttribute("used");
    mat[0][1] = document.getElementById("a2").getAttribute("used");
    mat[0][2] = document.getElementById("a3").getAttribute("used");
    mat[1][0] = document.getElementById("b1").getAttribute("used");
    mat[1][1] = document.getElementById("b2").getAttribute("used");
    mat[1][2] = document.getElementById("b3").getAttribute("used");
    mat[2][0] = document.getElementById("c1").getAttribute("used");
    mat[2][1] = document.getElementById("c2").getAttribute("used");
    mat[2][2] = document.getElementById("c3").getAttribute("used");

    // Verifica vitorias com a1
    if(mat[0][0] != ""){
        if ((mat[0][0] == mat[0][1] && mat[0][0] == mat[0][2]) || (mat[0][0] == mat[1][0] && mat[0][0] == mat[2][0]) || (mat[0][0] == mat[1][1] && mat[0][0] == mat[2][2])){
            winner = mat[0][0];
        }
    }
    // Verifica vitorias com c3
    if(mat[2][2] != ""){
        if ((mat[2][2] == mat[2][1] && mat[2][2] == mat[2][0]) || (mat[2][2] == mat[1][2] && mat[2][2] == mat[0][2])){
            winner = mat[2][2];
        }
    }
    // Verifica vitorias com b2
    if(mat[1][1] != ""){
        if ((mat[1][1] == mat[0][1] && mat[1][1] == mat[2][1]) || (mat[1][1] == mat[1][0] && mat[1][1] == mat[1][2]) || (mat[1][1] == mat[0][2] && mat[1][1] == mat[2][0])){
            winner = mat[1][1];
        }
    }
    // Verifica se deu velha
    var aux = 0;
    for (var m=0; m<3; m++){
        for (var n=0; n<3; n++){
            if (mat[m][n] == ""){
                aux++;
            }
        }
    }
    if (aux ==0){
        draw = true;
    }
    // Informa o vencedor
    if(winner != ""){
        gameover = true;
        if (winner == p1){
            ptp1++;
        }
        if (winner == p2){
            ptp2++;
        }
        await sleep(50);
        alert("Team " + winner + " venceu a rodada!");
        resetAll();
    }
    // Informa que deu velha
    else if(draw == true){
        gameover = true;
        await sleep(50);
        alert("A rodada empatou!");
        resetAll();
    }

}

function sleep(t){
    return new Promise(resolve => setTimeout(resolve, t));
}

function resetAll(){
    turn = p1;
    gameover = false;
    var field = document.getElementsByClassName("field");
    for (var i=0; i<field.length;i++){
         field[i].innerHTML = "";
         field[i].setAttribute("used", "");
    }
    main();
}

function restartMatch(){
    var bt = document.getElementById("rematch");
    bt.addEventListener("click", function(){
        document.location.reload(true)});
}

function actionOnTable(){
    // Cria um array com todos os campos do tabuleiro
    var field = document.getElementsByClassName("field");
    // Looping para verificar clique em cada espaço
    for (var i=0; i<field.length;i++){
        field[i].addEventListener("click", function(){
            if (gameover){
                return;
            }
            // Coloca a imagem e muda a vez do jogador
            if(this.getElementsByTagName("img").length == 0){
                if (turn == p1){
                    this.innerHTML = "<img src='img/teamaqua.png' border='0' height='50'>";
                    this.setAttribute("used", p1);
                    turn = p2;
                }
                else{
                    this.innerHTML = "<img src='img/teammagma.png' border='0' height='50'>";
                    this.setAttribute("used", p2);
                    turn = p1;
                }
                // Atualiza indicador de vez e verifica se o jogo acabou
                updateScore();
                endgame();
            }
        }
        );
    }

}


function updateScore(){
    // Verifica se o jogo ainda é válido
    if (gameover){
        return;
    }
    // Verifica o turno e coloca a imagem
    if (turn == p1){
        var playerimg = document.querySelectorAll("div#round img")[0];
        playerimg.setAttribute("src", "img/teamaqua.png");
    }
    else{
        var playerimg = document.querySelectorAll("div#round img")[0];
        playerimg.setAttribute("src", "img/teammagma.png");
    }
    //Acerta o placar
    var score1 = document.getElementById("scoreleft");
    var score2 = document.getElementById("scoreright");
    score1.innerHTML = "<p>Team Aqua: " + ptp1 + "</p>";
    score2.innerHTML = "<p>Team Magma: " + ptp2 + "</p>";
}

